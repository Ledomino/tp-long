package com.example.tplongii;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.example.tplongii.Entity.Compte;
import com.example.tplongii.Entity.Exercice;
import com.example.tplongii.Fragement.AjouterActivity;
import com.example.tplongii.Fragement.AjouterSeance;
import com.example.tplongii.Fragement.ModifyActivity;

import java.io.Console;

public class ExerciceActivity extends AppCompatActivity implements View.OnClickListener {

    private FragmentManager fm;
    private FragmentTransaction fragmentTransaction;
    private Compte compte_user;
    private TableLayout tab ;
    private Button modify ;
    private Button addSeance;
    private Button addExo ;
    private AjouterActivity addActivityFrag ;
    private AjouterSeance  addSeanceActivity;
    public final static int BTN_MODIF = 4598;
    public final static int BTN_ADD_SEANCE  = 5432;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exercice);

        String user = getIntent().getStringExtra("namelogin");
        compte_user = new Compte(user);
        addActivityFrag = new AjouterActivity(compte_user,this);
        Exercice Affexo = new Exercice(compte_user);

        fm = this.getSupportFragmentManager();
        fragmentTransaction = fm.beginTransaction();
        fragmentTransaction.replace(R.id.fragment_container,addActivityFrag);
        fragmentTransaction.commit();


        tab = findViewById(R.id.tabLayout);
        tab.removeAllViews();
        addExo = findViewById(R.id.btnAddExo);
        addExo.setOnClickListener(this);



        Affexo.afficheData(this);
    }

    @Override
    public void onClick(View view) {
        int pk = 0;
        int idLine = 0;

       if(view.getId() != R.id.btnAddExo) {
            pk = Integer.valueOf(view.getTag(R.string.pk) + "");
            idLine = Integer.valueOf(view.getTag(R.string.idLine) + "");
       }
        switch (view.getId())
        {
            case R.id.btnAddExo:
                fragmentTransaction = fm.beginTransaction();
                fragmentTransaction.replace(R.id.fragment_container, addActivityFrag);
                fragmentTransaction.commit();
                break;
            case BTN_MODIF:

                View viewRow = tab.getChildAt(0);
                View vText = ((TableRow) viewRow).getChildAt(0);

                ModifyActivity modAct = new ModifyActivity(this,this.compte_user,idLine,pk);
                fragmentTransaction = fm.beginTransaction();
                fragmentTransaction.replace(R.id.fragment_container, modAct);
                fragmentTransaction.commit();
                Log.d("TP",modAct + "");
                break;
            case BTN_ADD_SEANCE:
                addSeanceActivity = new AjouterSeance(this,idLine,compte_user);
                addSeanceActivity.ajouterExercice(idLine,compte_user);
                addSeanceActivity.setPK(pk);

                fragmentTransaction = fm.beginTransaction();
                fragmentTransaction.replace(R.id.fragment_container,addSeanceActivity );
                fragmentTransaction.commit();

                break;
        }
    }


    public void clearTable()
    {
        tab.removeAllViews();
    }

    public View getRow(int indice)
    {

        return tab.getChildAt(indice);
    }

    public void ajouterLigne(TableRow rows) {
        tab.addView(rows);
    }
}
