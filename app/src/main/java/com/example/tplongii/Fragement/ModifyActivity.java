package com.example.tplongii.Fragement;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TableRow;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.example.tplongii.Entity.Compte;
import com.example.tplongii.Entity.Exercice;
import com.example.tplongii.ExerciceActivity;
import com.example.tplongii.R;

public class ModifyActivity extends Fragment implements View.OnClickListener{


    private ExerciceActivity exoApp ;
    private String nomExercice , typeExercice;
    private int poids ,idLine , pk;
    private EditText editName , editType , editPoids ;
    private Button btnClear , btnSave ;
    private Compte auteur ;
    private AppCompatActivity app;
    private Spinner spinner;
    private View vRow ;

    public ModifyActivity(ExerciceActivity exoApp, Compte auteur, int idLine , int pk) {
        this.exoApp = exoApp;
        this.pk = pk;
        this.auteur = auteur ;
        vRow = exoApp.getRow(idLine);
        View vName = ((TableRow) vRow).getChildAt(0);
        View vType = ((TableRow) vRow).getChildAt(1);
        View vPoids = ((TableRow) vRow).getChildAt(2);

        this.nomExercice = ((TextView) vName ).getText().toString();
        this.typeExercice = ((TextView) vType ).getText().toString();
        this.poids = Integer.valueOf(((TextView) vPoids ).getText().toString());
       // this.poids = ((TextView) vPoids ).getText().toString();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_modify, container, false);
        int pos ;
        MySpinnerAdapter adapter = new MySpinnerAdapter(this.getContext());
        Log.d("TP_long","fragment modifier activité");
        editName = v.findViewById(R.id.txtNameExercice);
       // editType = v.findViewById(R.id.txtType);
        editPoids = v.findViewById(R.id.txtPoids);

        editName.setText(this.nomExercice);
      //  editType.setText(this.typeExercice);
        editPoids.setText(this.poids + "");

        this.btnClear = v.findViewById(R.id.btnclear);
        this.btnSave = v.findViewById(R.id.btnSaveMod);

        this.btnSave.setOnClickListener(this);
        btnClear.setOnClickListener(this);
        spinner = v.findViewById(R.id.txtType);
        spinner.setAdapter(adapter);

        pos = adapter.getPosition(this.typeExercice);
        spinner.setSelection(pos);
        Log.d("TP",pos + "");
        // Inflate the layout for this fragment
        return v;
    }

    @Override
    public String toString() {
        return "ModifyActivity{" +
                "nomExercice='" + nomExercice + '\'' +
                ", typeExercice='" + typeExercice + '\'' +
                ", poids=" + poids +
                '}';
    }

    @Override
    public void onClick(View view) {



        Exercice exo = new Exercice(spinner.getSelectedItem() + "",this.editName.getText().toString(), this.auteur,Integer.valueOf(this.editPoids.getText().toString()));
        switch (view.getId()){
            case R.id.btnclear:
               Exercice.delete(this.exoApp.getApplication(),this.pk);

                break;
            case R.id.btnSaveMod:

                exo.update(exoApp.getApplication(),pk + "");

                break;
        }
        exo.afficheData(this.exoApp);

    }
}
