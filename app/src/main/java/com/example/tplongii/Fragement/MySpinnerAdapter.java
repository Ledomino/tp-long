package com.example.tplongii.Fragement;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import com.example.tplongii.TypeExercice;

import java.util.ArrayList;
import java.util.List;

public class MySpinnerAdapter extends BaseAdapter implements SpinnerAdapter {

    private List<TypeExercice> items; // replace MyListItem with your model object type
    private Context context;

    public MySpinnerAdapter(Context aContext) {
        context = aContext;
        items = new ArrayList<TypeExercice>();
        items.add(null);
        // add first dummy item - selection of this will be ignored
        // TODO: add other items;
        items.add(TypeExercice.bras);
        items.add(TypeExercice.tors);
        items.add(TypeExercice.jambe);
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int aPosition) {
        return items.get(aPosition);
    }

    @Override
    public long getItemId(int aPosition) {
        return aPosition;
    }

    @Override
    public View getView(int aPosition, View aView, ViewGroup aParent) {
        TextView text = new TextView(context);
        if (aPosition == 0) {
            text.setText("-- Please select --"); // text for first dummy item

        } else {
            text.setText(items.get(aPosition).toString());
            // or use whatever model attribute you'd like displayed instead of toString()
        }
        return text;
    }

    public int getPosition(String nameItem) {

        int pos = -1 ;
        Log.d("TP_LONG",nameItem);
        for (int i = 1 ; i < items.size() ; i++)
        {
            Log.d("TP_LONG",items.get(i).name());

            if(items.get(i).name().contains(nameItem)) pos = i;
        }

        return pos;
    }
}