package com.example.tplongii.Fragement;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.example.tplongii.ActivityIII;
import com.example.tplongii.Entity.Compte;
import com.example.tplongii.Entity.Exercice;
import com.example.tplongii.Entity.Seance;
import com.example.tplongii.ExerciceActivity;
import com.example.tplongii.R;

import java.util.ArrayList;

public class AjouterSeance extends Fragment implements View.OnClickListener {


    private static ArrayList<Exercice> listExercice =  new ArrayList() ;
    private ExerciceActivity exoApp ;
    private TextView txtName ;
    private EditText editNbSeance ;
    private Button btnActIII ;
    private Button btnConfirmation;
    private Compte compte ;
    private int pk , idLine ;

    public AjouterSeance(ExerciceActivity exoActivity, int idLine, Compte compte) {

        this.exoApp = exoActivity;
        this.compte = compte;
        this.idLine = idLine;

    }

    public void ajouterExercice(int idLine, Compte compte)
    {
        View vRow = exoApp.getRow(idLine);
        View vName = ((TableRow) vRow).getChildAt(0);
        View vType = ((TableRow) vRow).getChildAt(1);
        View vPoids = ((TableRow) vRow).getChildAt(2);

        String nomExercice = ((TextView) vName ).getText().toString();
        String typeExercice = ((TextView) vType ).getText().toString();
        String poids = ((TextView) vPoids ).getText().toString();

        this.listExercice.add(new Exercice(typeExercice,nomExercice,compte,Integer.valueOf(poids)));
        listeExercice();
    }

    public void setPK(int pk)
    {
        this.pk = pk ;
    }


    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_ajouter_seance, container, false);
        Log.d("fragment", "test");
        btnActIII = v.findViewById(R.id.btnToActIII);
        btnActIII.setOnClickListener(this);
        txtName = v.findViewById(R.id.txtNameExercice);
        editNbSeance = v.findViewById(R.id.editNbRepetition);
        btnConfirmation = v.findViewById(R.id.btnConfirmation);
        btnConfirmation.setOnClickListener(this);
        loadView();

        if ((Seance.getNbRow(this.exoApp.getApplication(),compte    ) == 0)) {
            btnActIII.setEnabled(false);
        } else {
            btnActIII.setEnabled(true);
        }

        // Inflate the layout for this fragment
        return v;
    }


    public void refreshView()
    {
        loadView();
    }

    private void loadView()
    {
        btnActIII.setEnabled(false);
        String titre = Exercice.getData(exoApp.getApplication(),this.pk,1);
        txtName.setText(txtName.getText() + ""+titre);
    }

    public void listeExercice()
    {
        Log.d("TP","-------------------------------");
        for(Exercice e : listExercice)
        {
            Log.d("TP",e+"");
        }
        Log.d("TP","-------------------------------");
    }


    public static void createTable()
    {

    }

    @Override
    public void onClick(View view) {
        int nbfois ;
        switch (view.getId())
        {

            case R.id.btnConfirmation:
                if(Seance.isPresent(exoApp.getApplication(),this.pk)) {
                    Toast.makeText(this.getContext(), "Cette exercice est bien ajouté à la seance", Toast.LENGTH_LONG).show();
                    nbfois = (editNbSeance.getText().toString().length() == 0) ? 0 :  Integer.valueOf(editNbSeance.getText().toString());
                    Seance.ajouterSeance(exoApp.getApplication(), new Exercice(this.pk,nbfois, compte));
                    btnActIII.setEnabled(true);
                }else {
                    Toast.makeText(getContext(), "cette exercice est déja dans la séance", Toast.LENGTH_LONG).show();
                }
                    break;
            case R.id.btnToActIII:
                Intent intent = new Intent(this.getContext(), ActivityIII.class);
                startActivity(intent);
                break;

        }
    }
}

