package com.example.tplongii.Fragement;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;


import com.example.tplongii.Entity.Compte;
import com.example.tplongii.Entity.Exercice;
import com.example.tplongii.ExerciceActivity;
import com.example.tplongii.R;

public class AjouterActivity extends Fragment implements View.OnClickListener {

    private Spinner spinnerType;
    private EditText txtNom , txtType , txtpoids ;
    private Button btnValider ;
    private Compte compte ;
    private ExerciceActivity exoApp;

    public AjouterActivity(Compte compte, ExerciceActivity exoApp) {
        this.compte = compte;
        this.exoApp = exoApp;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_ajouter,container,false);
        spinnerType = v.findViewById(R.id.spinnerType);

        txtNom = v.findViewById(R.id.txtNameExercice);
        txtpoids = v.findViewById(R.id.txtPoids);
        btnValider = v.findViewById(R.id.btnSave);
        btnValider.setOnClickListener(this);

        spinnerType.setAdapter(new MySpinnerAdapter(this.getContext()));

        return v;

    }

    @Override
    public void onClick(View view) {
        Exercice exo = new Exercice(spinnerType.getSelectedItem()+ "",txtNom.getText() + "",compte,Integer.valueOf(txtpoids.getText()+""));
        exo.save(exoApp);
        exo.afficheData(exoApp);

    }
}
