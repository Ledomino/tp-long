package com.example.tplongii;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.example.tplongii.Entity.Compte;
import com.example.tplongii.Entity.Exercice;
import com.example.tplongii.Entity.Seance;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {


    private Button btnValider ;
    private EditText txtLogin , txtPasswd ;
    private Button btnInit ;
    private Compte data ;
    private Switch isConnexion ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnValider = findViewById(R.id.btnValider);
        btnValider.setOnClickListener(this);
        btnInit = findViewById(R.id.btnInit);
        btnInit.setOnClickListener(this);

        txtLogin = findViewById(R.id.txtLogin);
        txtPasswd = findViewById(R.id.txtpasswd);
        isConnexion = findViewById(R.id.switchMode);

    }

    public void init()
    {
        Compte.clear(this.getApplication());
        Exercice.clear(this.getApplication());
        Seance.deleteTable(this.getApplication());
        Compte user = new Compte("User1","1234");
        Seance.createTable(this.getApplication());
        Exercice exo = new Exercice(TypeExercice.jambe.toString(),"demo exo",user,12);
        Compte.createTable(this);
        Exercice.createTable(this);

        user.enregistreUtilisateur(this);
        exo.save(this);

    }

    public void reset()
    {}

    @Override
    public void onClick(View view) {

        TextView txt = findViewById(R.id.txtOutput);
        String login = txtLogin.getText() + "";
        String passwd = txtPasswd.getText().toString() ;
        data = new Compte(login,passwd);
        if(view.getId() == R.id.btnInit)
        {
            init();
            Toast.makeText(getApplicationContext(), "Reinitialiasation de la BD", Toast.LENGTH_LONG).show();
            return ;
        }

        if (isConnexion.isChecked()) {
            if(data.login(this)) {
                Intent intent = new Intent(this,ExerciceActivity.class);
                intent.putExtra("namelogin",login);
                startActivity(intent);
                txt.setText("ok");
            }else
            {
                txt.setText("mauvaise login/passwd");
            }
        } else {
            txt.setText(data.enregistreUtilisateur(this));

        }

    }
}
