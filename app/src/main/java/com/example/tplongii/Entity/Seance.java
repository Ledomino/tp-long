package com.example.tplongii.Entity;

import android.app.Application;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.widget.TableRow;

import androidx.appcompat.app.AppCompatActivity;

public class Seance {


    private int idExo ;
    private Compte compte ;
    private int nbFois ;
    private final static String TABLE_NAME = "Seance";
    private final static String DB_NAME = "tplong";


    public Seance(int exo, Compte compte, int nbFois) {
        this.idExo = exo;
        this.compte = compte;
        this.nbFois = nbFois;
    }


    public static void createTable(Application app)
    {
        String sql  = "create table if not exists "+ TABLE_NAME+ " (id integer primary key AUTOINCREMENT, idExercice integer, utilisateur varchar(20) , nbFois integer)";
        SQLiteDatabase base  = app.openOrCreateDatabase(DB_NAME, AppCompatActivity.MODE_PRIVATE,null);
        base.execSQL(sql);
        base.close();
    }

    public static boolean isPresent(Application app, int pk)
    {
        String sql = "select * from "+TABLE_NAME+" where id = "+pk;
        SQLiteDatabase base  = app.openOrCreateDatabase(DB_NAME, AppCompatActivity.MODE_PRIVATE,null);
         Cursor cursor = base.rawQuery(sql, null);
         cursor.moveToFirst();
         return cursor.getCount() == 0;

    }



    public static int getNbRow(Application app,Compte compte)
    {
        String sql = "select * from "+TABLE_NAME+ " where utilisateur = '"+compte.getUser()+"'";
        SQLiteDatabase base  = app.openOrCreateDatabase(DB_NAME, AppCompatActivity.MODE_PRIVATE,null);
        Cursor cursor = base.rawQuery(sql, null);
        cursor.moveToFirst();
        return cursor.getCount();
    }



    public static void deleteTable(Application app)
    {
        String sql = "drop table if exists "+TABLE_NAME;
        SQLiteDatabase base = app.openOrCreateDatabase(DB_NAME,AppCompatActivity.MODE_PRIVATE,null);
        base.execSQL(sql);
        base.close();
    }

    public static void ajouterSeance(Application app , Exercice exo)
    {
        ContentValues val = new ContentValues();
        val.put("idExercice",exo.getPk());
        val.put("utilisateur",exo.getAuteur().getUser());
        val.put("nbFois",exo.getNbFois()) ;

        SQLiteDatabase base = app.openOrCreateDatabase(DB_NAME,AppCompatActivity.MODE_PRIVATE,null);
        base.insert(TABLE_NAME,null,val);
        base.close();
    }
}
