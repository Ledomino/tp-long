package com.example.tplongii.Entity;

import android.app.Application;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.util.Log;
import android.util.TypedValue;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TableRow;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.tplongii.ExerciceActivity;
import com.example.tplongii.R;

import java.security.AccessControlContext;

import static java.security.AccessController.getContext;

public class Exercice {

    private String type ;
    private String nomExercice;
    private Compte auteur ;
    private int poids ;
    private int nbFois ;
    private final static String TABLE_NAME = "exercice";
    private final static String DB_NAME = "tplong";
    private  Float scale;
    private int id ;
    private int pk ;
    private static int count = 0;


    public Exercice(String type, String nomExercice, Compte auteur, int poids) {
        this.type = type;
        this.nomExercice = nomExercice;
        this.auteur = auteur;
        this.poids = poids;



    }
    public Exercice(int pk , int nbFois , Compte compte)
    {
        this.pk = pk ;
        this.nbFois = nbFois;
        this.auteur = compte;
    }


    public Compte getAuteur() {
        return auteur;
    }

    public int getNbFois() {
        return nbFois;
    }

    public int getPk() {
        return pk;
    }

    public Exercice(Compte auteur) {
        this.auteur = auteur;
    }


    public void save(AppCompatActivity app)
    {
        ContentValues val = new ContentValues();
        val.put("nomExercice",this.nomExercice);
        val.put("typeExercice",this.type);
        val.put("auteur",this.auteur.getUser());
        val.put("poids",this.poids);
        SQLiteDatabase base = app.getApplication().openOrCreateDatabase(DB_NAME,AppCompatActivity.MODE_PRIVATE,null);
        base.insert(TABLE_NAME,null,val);
        base.close();
    }

    public  void afficheData(ExerciceActivity app) {
        int idRow = 0;
        String sql = "select * from " + TABLE_NAME + " where auteur = '" + this.auteur.getUser() + "'";
        SQLiteDatabase base = app.getApplication().openOrCreateDatabase(DB_NAME, AppCompatActivity.MODE_PRIVATE, null);
        app.clearTable();

        try {
            Cursor cursor = base.rawQuery(sql, null);
            cursor.moveToFirst();
            Log.d("TP", "----------------------------");
            if (cursor.getCount() == 0) return;
            do {
                id = cursor.getInt(0);

                Log.d("TP", "Name :" + cursor.getString(1));
                Log.d("TP", "Type :" + cursor.getString(2));
                Log.d("TP", "poid :" + cursor.getString(4));
                Log.d("TP","id : " + id);


                TableRow rows = new TableRow(app.getApplicationContext());
                Button modify = new Button(app.getApplicationContext());
                Button addSeance = new Button(app.getApplicationContext());

                TextView txtPoids = new TextView(app.getApplicationContext());
                txtPoids.setText(cursor.getString(4));
                txtPoids.setWidth(0);

                modify.setText("modification");
                modify.setTag(R.string.pk,id);
                modify.setTag(R.string.idLine,idRow);
                addSeance.setText("AjouterSeance");
                addSeance.setTag(R.string.pk,id);
                addSeance.setTag(R.string.idLine,idRow);

                modify.setId(ExerciceActivity.BTN_MODIF);
                addSeance.setId(ExerciceActivity.BTN_ADD_SEANCE);

                TextView txtName = new TextView(app.getApplicationContext());
                TextView txtType = new TextView(app.getApplicationContext());

                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,LinearLayout.LayoutParams.WRAP_CONTENT);



                android.widget.TableRow.LayoutParams p = new android.widget.TableRow.LayoutParams();
                p.rightMargin = dpToPixel(10, app.getApplicationContext()); // right-margin = 10dp

                txtName.setText(cursor.getString(1));
                txtName.setLayoutParams(p);
                txtType.setText(cursor.getString(2));


                txtName.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 14);
                txtType.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 14);
                modify.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 12);
                addSeance.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 12);

                modify.setOnClickListener(app);
                addSeance.setOnClickListener(app);
                rows.addView(txtName);
                rows.addView(txtType);
                rows.addView(txtPoids);
                rows.addView(modify);
                rows.addView(addSeance);
                rows.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT, 1));


                app.ajouterLigne(rows);

                idRow++;
            } while (cursor.moveToNext());
            Log.d("TP", "----------------------------");
        } catch (SQLiteException e) {
            Log.d("TP", e.getMessage() + "");
        }

    }

    private  int dpToPixel(int dp, Context context) {
        if (scale == null)
            scale = context.getResources().getDisplayMetrics().density;
        return (int) ((float) dp * scale);
    }

    public static void createTable(AppCompatActivity app)
    {
        String sql  = "create table if not exists "+TABLE_NAME+" (id integer primary key AUTOINCREMENT, nomExercice varcher(30), typeExercice varchar(20),auteur varchar(30),poids integer)";
        SQLiteDatabase base  = app.openOrCreateDatabase(DB_NAME, AppCompatActivity.MODE_PRIVATE,null);
        base.execSQL(sql);
        base.close();
    }

    public static void clear(Application app)
    {
        String sql = "drop table if exists "+TABLE_NAME;
        SQLiteDatabase base = app.openOrCreateDatabase(DB_NAME,AppCompatActivity.MODE_PRIVATE,null);
        base.execSQL(sql);
        base.close();
    }

    public static void delete(Application app, int indice)
    {
        String sql = "delete from "+TABLE_NAME+ " where id = "+indice;
        SQLiteDatabase base = app.openOrCreateDatabase(DB_NAME,AppCompatActivity.MODE_PRIVATE,null);
        base.execSQL(sql);
        base.close();
    }


    public static String getData(Application app , int pk , int columns)
    {
        String sql = "Select * from "+TABLE_NAME+" where id = "+pk;
        String data ;
        SQLiteDatabase base = app.openOrCreateDatabase(DB_NAME,AppCompatActivity.MODE_PRIVATE,null);
        Cursor cursor = base.rawQuery(sql, null);
        cursor.moveToFirst();
        data = cursor.getString(columns);
        base.close();
        return data;
    }


    public  void update(Application app, String pk)
    {
        ContentValues val = new ContentValues();
        val.put("nomExercice",this.nomExercice);
        val.put("typeExercice",this.type);
        val.put("poids",this.poids);
        Log.d("TP","methode update");
        Log.d("TP","id : "+pk);

        SQLiteDatabase base = app.openOrCreateDatabase(DB_NAME,AppCompatActivity.MODE_PRIVATE,null);
       long i =  base.update(TABLE_NAME,val," id = ?",new String[] {pk});
       if(i>0)
       {
           Log.d("TP","successful");
       }else{
           Log.d("TP","Bug Grr:(");
       }
    }

    @Override
    public String toString() {
        return "Exercice{" +
                "type='" + type + '\'' +
                ", nomExercice='" + nomExercice + '\'' +
                ", auteur=" + auteur.getUser() +
                ", poids=" + poids +
                '}';
    }
}
