package com.example.tplongii.Entity;

import android.app.Application;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import androidx.appcompat.app.AppCompatActivity;

public class Compte {

    private final static String TABLE_NAME = "Utilisateur";
    private final static String DB_NAME = "tplong";
    private String user ;
    private String passwd ;
    private int idUser ;

    public Compte(String user,String passwd)
    {
        this.user = user;
        this.passwd = passwd;
    }

    public Compte(String user){
        this.user = user ;
    }

    public boolean login(AppCompatActivity app)
    {
        SQLiteDatabase database = app.openOrCreateDatabase(DB_NAME, AppCompatActivity.MODE_PRIVATE,null);
        String sql = "select * from %s where pseudo = '%s' and passwd = '%s'";
        sql = String.format(sql,TABLE_NAME,user,passwd);



        return database.rawQuery(sql,null).getCount() == 1;
    }



    public static void clear(Application app)
    {
        String sql = "drop table if exists "+TABLE_NAME;
        SQLiteDatabase base = app.openOrCreateDatabase(DB_NAME,AppCompatActivity.MODE_PRIVATE,null);
        base.execSQL(sql);
        base.close();
    }

    public static void createTable(AppCompatActivity app)
    {
        SQLiteDatabase database = app.openOrCreateDatabase(DB_NAME, AppCompatActivity.MODE_PRIVATE,null);
        database.execSQL("create table if not exists "+TABLE_NAME+"(pseudo varchar(20) primary key, passwd varchar(20))");
        database.close();
    }

    public String enregistreUtilisateur(AppCompatActivity app)
    {
        ContentValues val = new ContentValues();
        SQLiteDatabase database = app.openOrCreateDatabase(DB_NAME, AppCompatActivity.MODE_PRIVATE,null);
        String sql = "select * from %s where pseudo = '%s'";
        sql = String.format(sql,TABLE_NAME,user);
        Cursor cursor  = database.rawQuery(sql,null);
        if(cursor.getCount() == 0) {
            val.put("pseudo", user);
            val.put("passwd", passwd);


            database.insert(TABLE_NAME, null, val);

            return "Ce compte est bien enregistré";
        }else{
            return "Ce compte est déja utilisé";
        }


    }


    public String getUser() {
        return user;
    }
}
